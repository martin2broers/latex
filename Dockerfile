FROM thomasweise/texlive

RUN apt-get update && apt-get install -y texlive-lang-european

VOLUME ["/doc/", "/usr/share/fonts/external/"]
ENTRYPOINT ["/bin/__boot__.sh"]